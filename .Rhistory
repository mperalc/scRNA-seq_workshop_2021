# This is for building the book, no need to run it
library(bookdown)
library(knitr)
# automatically create a bib database for R packages
knitr::write_bib(c(
.packages(), 'bookdown', 'knitr', 'rmarkdown',"Seurat","limma","tidyverse","patchwork","future","DoubletFinder"
), 'packages.bib')
img1_path <- "data/figures/technologies-papalexi.png"
include_graphics(img1_path)
img1_path <- "data/figures/technologies-See.png"
include_graphics(img1_path)
# Load the PBMC dataset
pbmc.data <- Read10X(data.dir = "data/raw_counts/filtered_gene_bc_matrices/hg19/")
# Initialize the Seurat object with the raw (non-normalized data).
pbmc <- CreateSeuratObject(counts = pbmc.data,
min.cells = 3,
project = "pbmc3k")
pbmc
# Lets examine a few genes in the first thirty cells
pbmc.data[c("CD3D", "MS4A1", "CD14"), 1:30]
dense.size <- object.size(as.matrix(pbmc.data))
dense.size
sparse.size <- object.size(pbmc.data)
sparse.size
as.integer(round(dense.size/sparse.size))
# Adding the percentage of mitochondrial genes to the metadata table
pbmc[["percent.mt"]] = PercentageFeatureSet(pbmc, pattern = "^MT-")
head(pbmc@meta.data)
#Visualize QC metrics as a violin plot
VlnPlot(pbmc, features = c("nFeature_RNA", "nCount_RNA", "percent.mt"), ncol = 3)
# FeatureScatter is typically used to visualize feature-feature relationships, but can be used for anything calculated by the object, i.e. columns in object metadata, PC scores etc.
plot1 = FeatureScatter(pbmc, feature1 = "nCount_RNA", feature2 = "percent.mt") + NoLegend()  +
ylab("% of mitochondrial genes") +
xlab("UMI counts") +
geom_hline(yintercept = 5)
plot2 = FeatureScatter(pbmc, feature1 = "nCount_RNA", feature2 = "nFeature_RNA") + NoLegend() +
ylab("Number of genes") +
xlab("UMI counts") +
geom_hline(yintercept = 200)
plot1 + plot2
img1_path = "data/figures/PTK2B_lib1_lib2_metrics_scatter.png"
include_graphics(img1_path)
img1_path = "data/figures/Osorio-mit.png"
include_graphics(img1_path)
mark_doublets = function(seurat_object){
message("Performing filter by number of genes and mitochondrial percentage.")
seurat_object = subset(seurat_object, subset = nFeature_RNA > 200  & percent.mt < 5)
message("Now the object has ", dim(seurat_object)[1], " genes and ", dim(seurat_object)[2], " cells.")
# SCTransform
# change the current plan to access parallelization
plan("multiprocess", workers = 4)
plan()
# Give more memory to globals to prevent crashing by large Seurat objects
options(future.globals.maxSize= 2097152000) # 2Gb
seurat_object = SCTransform(seurat_object, verbose = FALSE)
seurat_object = RunPCA(seurat_object, verbose = FALSE)
seurat_object = FindNeighbors(seurat_object, dims = 1:30, verbose = FALSE)
seurat_object = FindClusters(seurat_object, verbose = FALSE)
seurat_object = RunUMAP(seurat_object, dims = 1:30, verbose = FALSE)
sweep.res.list = paramSweep_v3(seurat_object, PCs = 1:10, sct = TRUE)
sweep.stats = summarizeSweep(sweep.res.list, GT = FALSE)
bcmvn = find.pK(sweep.stats)
# This plot will help you find the correct value for pK
ggplot(bcmvn,aes(x=pK,y=BCmetric, group = 1)) + geom_line() + geom_point()
# select pK that maximizes the BCmetric
pk = bcmvn[which(bcmvn$BCmetric == max(bcmvn$BCmetric)),"pK"]
message("Your best pK value is ", pk)
# Select a number of predicted homotypic doublets:
nExp_poi = round(0.075*nrow(seurat_object@meta.data))  ## Assuming 7.5% doublet formation rate - tailor for your dataset
# This step is problematic
seurat_object.doublet =
doubletFinder_v3(
seurat_object,
PCs = 1:10,
pN = 0.25,
pK = 0.04,
nExp = nExp_poi,
reuse.pANN = FALSE,
sct = TRUE
)
return(seurat_object.doublet)
}
pbmc.doublet = mark_doublets(pbmc)
# Saving only doublet information columns
write.csv(pbmc.doublet@meta.data[(ncol(pbmc.doublet@meta.data)-1):ncol(pbmc.doublet@meta.data)],
file = "data/results/pbmc_doublets.csv")
# Read in results from doublet calling
doublets = read.csv("data/results/pbmc_doublets.csv", row.names = 1)
# Add predicted doublets to metadata
pbmc@meta.data$Doublet_DF = "Singlet"
pbmc@meta.data[rownames(doublets[doublets$DF.classifications_0.25_0.04_201 == "Doublet",]),"Doublet_DF"] = "Doublet"
message("There are ", sum(pbmc@meta.data$Doublet_DF=="Doublet"), " predicted doublets in our data")
message("That's ", floor((sum(pbmc@meta.data$Doublet_DF=="Doublet")/nrow(pbmc@meta.data))*100), "% of our our data")
doublets_hists = function(seurat_object) {
plot1 <- seurat_object@meta.data %>%
ggplot(aes(color = Doublet_DF, x = nCount_RNA, fill = Doublet_DF)) +
geom_density(alpha = 0.2) +
scale_x_log10() +
theme_classic() +
ylab("Cell density") +
xlab("UMI counts")
plot2 <- seurat_object@meta.data %>%
ggplot(aes(color = Doublet_DF, x = nFeature_RNA, fill = Doublet_DF)) +
geom_density(alpha = 0.2) +
theme_classic() +
scale_x_log10() +
ylab("Cell density") +
xlab("Number of genes") +
geom_vline(xintercept = 200)
plot3 <- seurat_object@meta.data %>%
ggplot(aes(color = Doublet_DF, x = percent.mt, fill = Doublet_DF)) +
geom_density(alpha = 0.2) +
theme_classic() +
geom_vline(xintercept = 15) +
ylab("Cell density") +
xlab("% of mitochondrial genes")
p = plot1 + plot2 + plot3 + plot_layout(guides = 'collect')
return(p)
}
p = doublets_hists(pbmc)
p
# Read in results from doublet calling
doublets = read.csv("data/results/pbmc_doublets.csv", row.names = 1)
head(doublets)
# Read in results from doublet calling
doublets = read.csv("data/results/pbmc_doublets.csv", row.names = 1)
# Add predicted doublets to metadata
pbmc@meta.data$Doublet_DF = "Singlet"
pbmc@meta.data[rownames(doublets[doublets[ncol(doublets)] == "Doublet",]),"Doublet_DF"] = "Doublet"
message("There are ", sum(pbmc@meta.data$Doublet_DF=="Doublet"), " predicted doublets in our data")
message("That's ", floor((sum(pbmc@meta.data$Doublet_DF=="Doublet")/nrow(pbmc@meta.data))*100), "% of our our data")
doublets_hists = function(seurat_object) {
plot1 <- seurat_object@meta.data %>%
ggplot(aes(color = Doublet_DF, x = nCount_RNA, fill = Doublet_DF)) +
geom_density(alpha = 0.2) +
scale_x_log10() +
theme_classic() +
ylab("Cell density") +
xlab("UMI counts")
plot2 <- seurat_object@meta.data %>%
ggplot(aes(color = Doublet_DF, x = nFeature_RNA, fill = Doublet_DF)) +
geom_density(alpha = 0.2) +
theme_classic() +
scale_x_log10() +
ylab("Cell density") +
xlab("Number of genes") +
geom_vline(xintercept = 200)
plot3 <- seurat_object@meta.data %>%
ggplot(aes(color = Doublet_DF, x = percent.mt, fill = Doublet_DF)) +
geom_density(alpha = 0.2) +
theme_classic() +
geom_vline(xintercept = 15) +
ylab("Cell density") +
xlab("% of mitochondrial genes")
p = plot1 + plot2 + plot3 + plot_layout(guides = 'collect')
return(p)
}
p = doublets_hists(pbmc)
p
calculate_SCT_PCA_UMAP_neighbors_clusters = function(seurat_object){
seurat_object = SCTransform(seurat_object, verbose = FALSE)
seurat_object <- RunPCA(seurat_object, verbose = FALSE)
seurat_object <- FindNeighbors(seurat_object, dims = 1:30, verbose = FALSE)
seurat_object <- FindClusters(seurat_object, verbose = FALSE)
seurat_object <- RunUMAP(seurat_object, dims = 1:30, verbose = FALSE)
return(seurat_object)
}
# Saving in a new object so we don't disturb the old un-transformed, unfiltered one
pbmc.doublet = calculate_SCT_PCA_UMAP_neighbors_clusters(pbmc)
doublets_UMAP = function(seurat_object) {
p1 = DimPlot(seurat_object, label = F,  group.by = "Doublet_DF")  + ggtitle("DoubletFinder")
p1[[1]]$layers[[1]]$aes_params$alpha = .4
# p2 = FeaturePlot(seurat_object, label = F,  features = "nCount_RNA")  + ggtitle("UMI counts")
# p2[[1]]$layers[[1]]$aes_params$alpha = .4
#
# p3 = FeaturePlot(seurat_object, label = F,  features = "nFeature_RNA")  + ggtitle("Gene number")
# p3[[1]]$layers[[1]]$aes_params$alpha = .4
p = p1
return(p)
}
p= doublets_UMAP(pbmc.doublet)
p
filter_seurat = function(seurat_object){
message("Performing filter by number of genes and mitochondrial percentage.")
seurat_object = subset(seurat_object, subset = nFeature_RNA > 200  & percent.mt < 5)
message("Now the object has ", dim(seurat_object)[1], " genes and ", dim(seurat_object)[2], " cells.")
return(seurat_object)
}
pbmc = filter_seurat(pbmc)
library(seurat)
library(Seurat)
?Read10X
?CreateSeuratObject
# Install these if you haven't done so already
# install.packages('Seurat')
# install.packages("tidyverse")
# install.packages("future")
# install.packages("clustree")
# if (!requireNamespace("BiocManager", quietly = TRUE))
#     install.packages("BiocManager")
# BiocManager::install("limma")
# install.packages("devtools")
# devtools::install_github("thomasp85/patchwork")
# devtools::install_github('chris-mcginnis-ucsf/DoubletFinder')
# For all parts
library(Seurat)
library(limma)
library(tidyverse) # For ggplot2 and easy manipulation of data
library(patchwork) # To combine plots
library(future) # For paralellization
library(DoubletFinder) # For finding  doublets
library(clustree) # To select clustering resolution
options(stringsAsFactors = FALSE) # Set this to deactivate the automatic read-in of characters as factors
# Load the PBMC dataset
pbmc.data <- Read10X(data.dir = "data/raw_counts/filtered_gene_bc_matrices/hg19/")
pbmc<- CreateSeuratObject(counts = pbmc.data$`Gene Expression`, project = "pbmc3k")
# Initialize the Seurat object with the raw (non-normalized data).
pbmc <- CreateSeuratObject(counts = pbmc.data,
min.cells = 3,
project = "pbmc3k")
pbmc
str(pbmc)
head(str(pbmc))
library(bookdown)
bookdown::serve_book()
# Load the PBMC dataset
pbmc.data <- Read10X(data.dir = "data/raw_counts/filtered_gene_bc_matrices/hg19/")
# Initialize the Seurat object with the raw (non-normalized data).
pbmc <- CreateSeuratObject(counts = pbmc.data,
min.cells = 3,
project = "pbmc3k")
# Adding the percentage of mitochondrial genes to the metadata table
pbmc[["percent.mt"]] = PercentageFeatureSet(pbmc, pattern = "^MT-")
pbmc
2700*2.3
6210/3000
img1_path <- "data/figures/technologies-papalexi.png"
include_graphics(img1_path)
img1_path <- "data/figures/technologies-See.png"
include_graphics(img1_path)
# Load the PBMC dataset
pbmc.data <- Read10X(data.dir = "data/raw_counts/filtered_gene_bc_matrices/hg19/")
# Initialize the Seurat object with the raw (non-normalized data).
pbmc <- CreateSeuratObject(counts = pbmc.data,
min.cells = 3,
project = "pbmc3k")
# Adding the percentage of mitochondrial genes to the metadata table
pbmc[["percent.mt"]] = PercentageFeatureSet(pbmc, pattern = "^MT-")
mark_doublets = function(seurat_object){
message("Performing filter by number of genes and mitochondrial percentage.")
seurat_object = subset(seurat_object, subset = nFeature_RNA > 200  & percent.mt < 5)
message("Now the object has ", dim(seurat_object)[1], " genes and ", dim(seurat_object)[2], " cells.")
# SCTransform
# change the current plan to access parallelization
plan("multiprocess", workers = 4)
plan()
# Give more memory to globals to prevent crashing by large Seurat objects
options(future.globals.maxSize= 2097152000) # 2Gb
seurat_object = SCTransform(seurat_object, verbose = FALSE)
seurat_object = RunPCA(seurat_object, verbose = FALSE)
seurat_object = FindNeighbors(seurat_object, dims = 1:30, verbose = FALSE)
seurat_object = FindClusters(seurat_object, verbose = FALSE)
seurat_object = RunUMAP(seurat_object, dims = 1:30, verbose = FALSE)
sweep.res.list = paramSweep_v3(seurat_object, PCs = 1:10, sct = TRUE)
sweep.stats = summarizeSweep(sweep.res.list, GT = FALSE)
bcmvn = find.pK(sweep.stats)
# This plot will help you find the correct value for pK
ggplot(bcmvn,aes(x=pK,y=BCmetric, group = 1)) + geom_line() + geom_point()
# select pK that maximizes the BCmetric
bcmvn$pK = as.numeric(as.character(bcmvn$pK))
pk = bcmvn[which(bcmvn$BCmetric == max(bcmvn$BCmetric)),"pK"]
message("Your best pK value is ", pk)
# Homotypic doublet proportion estimation - from number of clusters
annotations =  as.character(seurat_object$seurat_clusters)
homotypic.prop <- modelHomotypic(annotations)
# Estimate number of total expected doublets
# Assuming 2.07% doublet formation rate (10X rate for 2700 recovered cells) - tailor for your dataset
expected_doublet_rate = 0.02
nExp_poi = round(expected_doublet_rate*nrow(seurat_object@meta.data))
# Adjusting number based on homotypic doublet proportion expected
nExp_poi.adj <- round(nExp_poi*(1-homotypic.prop))
# All confidence doublets - not adjusted for homotypic doublets
seurat_object.doublet <-
doubletFinder_v3(
seurat_object,
PCs = 1:10,
pN = 0.25,
pK = pk,
nExp = nExp_poi,
reuse.pANN = FALSE,
sct = TRUE
)
# High confidence doublets - adjusted for homotypic doublets
seurat_object.doublet <-
doubletFinder_v3(
seurat_object.doublet,
PCs = 1:10,
pN = 0.25,
pK = pk,
nExp = nExp_poi.adj,
reuse.pANN = colnames(seurat_object.doublet@meta.data)[ncol(seurat_object.doublet@meta.data)-1],
sct = TRUE
)
# Integrate info all and high confidence
seurat_object.doublet@meta.data$DF_confidence = "Singlet"
seurat_object.doublet@meta.data[seurat_object.doublet[[colnames(seurat_object.doublet@meta.data)[ncol(seurat_object.doublet@meta.data) -
2]]] == "Doublet" &
seurat_object.doublet[[colnames(seurat_object.doublet@meta.data)[ncol(seurat_object.doublet@meta.data)-1]]] == "Doublet",
"DF_confidence"] = "High_confidence_doublet"
seurat_object.doublet@meta.data[seurat_object.doublet[[colnames(seurat_object.doublet@meta.data)[ncol(seurat_object.doublet@meta.data) -
2]]] == "Doublet" &
seurat_object.doublet[[colnames(seurat_object.doublet@meta.data)[ncol(seurat_object.doublet@meta.data)-1]]] == "Singlet",
"DF_confidence"] = "Low_confidence_doublet"
return(seurat_object.doublet)
}
pbmc.doublet = mark_doublets(pbmc)
# Saving only doublet information columns
write.csv(pbmc.doublet@meta.data[(ncol(pbmc.doublet@meta.data)-1):ncol(pbmc.doublet@meta.data)],
file = "data/results/pbmc_doublets.csv")
head(pbmc.doublet)
head(pbmc.doublet@meta.data)
check = pbmc.doublet@meta.data
View(check)
# Read in results from doublet calling
doublets = read.csv("data/results/pbmc_doublets.csv", row.names = 1)
head(doublets)
# Add predicted doublets to metadata
pbmc@meta.data$DF_confidence = "Singlet"
pbmc@meta.data[rownames(doublets[doublets[ncol(doublets)] == "High_confidence_doublet",]),"DF_confidence"] = "High_confidence_doublet"
pbmc@meta.data[rownames(doublets[doublets[ncol(doublets)] == "Low_confidence_doublet",]),"DF_confidence"] = "Low_confidence_doublet"
message("There are ", sum(pbmc@meta.data$DF_confidence!="Singlet"), " predicted doublets in our data")
message("There are ", sum(pbmc@meta.data$DF_confidence!="Singlet"), " predicted doublets in our data")
message("That's ", floor((sum(pbmc@meta.data$DF_confidence!="Singlet")/nrow(pbmc@meta.data))*100), "% of our our data")
calculate_SCT_PCA_UMAP_neighbors_clusters = function(seurat_object){
seurat_object = SCTransform(seurat_object, verbose = FALSE)
seurat_object <- RunPCA(seurat_object, verbose = FALSE)
seurat_object <- FindNeighbors(seurat_object, dims = 1:30, verbose = FALSE)
seurat_object <- FindClusters(seurat_object, verbose = FALSE)
seurat_object <- RunUMAP(seurat_object, dims = 1:30, verbose = FALSE)
return(seurat_object)
}
# Saving in a new object so we don't disturb the old un-transformed, unfiltered one
pbmc.doublet = calculate_SCT_PCA_UMAP_neighbors_clusters(pbmc)
p = DimPlot(
pbmc.doublet,
label = F,
group.by = "DF_confidence",
cols = c(
'Singlet' = '#28C2FF',
'High_confidence_doublet' = '#BD6B73',
'Low_confidence_doublet' = '#FFA630'
)
) +
ggtitle("DoubletFinder doublets")
p
png("data/results/PBMC_UMAP_doublets.png",
width = 10, height = 10, units = "in", res = 400)
p
dev.off()
png("data/results/PBMC_UMAP_doublets.png",
width = 8, height = 6, units = "in", res = 400)
p
dev.off()
p = p+ p[[1]]$layers[[1]]$aes_params$alpha = .1
p = p[[1]]$layers[[1]]$aes_params$alpha = .1
p
p = DimPlot(
pbmc.doublet,
label = F,
group.by = "DF_confidence",
cols = c(
'Singlet' = '#28C2FF',
'High_confidence_doublet' = '#BD6B73',
'Low_confidence_doublet' = '#FFA630'
)
) +
ggtitle("DoubletFinder doublets")
p$layers[[1]]$aes_params$alpha = .1
p
p = DimPlot(
pbmc.doublet,
label = F,
group.by = "DF_confidence",
cols = c(
'Singlet' = '#28C2FF',
'High_confidence_doublet' = '#BD6B73',
'Low_confidence_doublet' = '#FFA630'
)
) +
ggtitle("DoubletFinder doublets")
png("data/results/PBMC_UMAP_doublets.png",
width = 8, height = 6, units = "in", res = 400)
p
dev.off()
p
p = DimPlot(
pbmc.doublet,
label = F,
group.by = "DF_confidence",
cols = c(
'Singlet' = '#28C2FF',
'High_confidence_doublet' = '#BD6B73',
'Low_confidence_doublet' = '#FFA630'
)
) +
ggtitle("DoubletFinder doublets")
# Add a bit of transparency
p$layers[[1]]$aes_params$alpha = .3
p
png("data/results/PBMC_UMAP_doublets.png",
width = 8, height = 6, units = "in", res = 400)
p
dev.off()
p = DimPlot(
pbmc.doublet,
label = F,
group.by = "DF_confidence",
cols = c(
'Singlet' = '#28C2FF',
'High_confidence_doublet' = '#BD6B73',
'Low_confidence_doublet' = '#FFA630'
)
) +
ggtitle("DoubletFinder doublets")
# Add a bit of transparency
p$layers[[1]]$aes_params$alpha = .4
png("data/results/PBMC_UMAP_doublets.png",
width = 6, height = 4, units = "in", res = 400)
p
dev.off()
p = DimPlot(
pbmc.doublet,
label = F,
group.by = "DF_confidence",
cols = c(
'Singlet' = '#28C2FF',
'High_confidence_doublet' = '#BD6B73',
'Low_confidence_doublet' = '#FFA630'
)
) +
ggtitle("DoubletFinder doublets")
# Add a bit of transparency to Singlets
p[[1]]$layers[[1]]$aes_params$alpha = ifelse ( pbmc.doublet@meta.data$DF_confidence == "Singlet", .4, 1 )
png("data/results/PBMC_UMAP_doublets.png",
width = 6, height = 4, units = "in", res = 400)
p
dev.off()
p
png("data/results/PBMC_UMAP_doublets.png",
width = 6, height = 4, units = "in", res = 400)
p
dev.off()
?DimPlot
p = DimPlot(
pbmc.doublet,
label = F,
group.by = "DF_confidence",
cols = c(
'Singlet' = '#28C2FF',
'High_confidence_doublet' = '#BD6B73',
'Low_confidence_doublet' = '#FFA630'
),
order = c('High_confidence_doublet','Low_confidence_doublet' )
) +
ggtitle("DoubletFinder doublets")
p
p = DimPlot(
pbmc.doublet,
label = F,
group.by = "DF_confidence",
cols = c(
'Singlet' = '#28C2FF',
'High_confidence_doublet' = '#BD6B73',
'Low_confidence_doublet' = '#FFA630'
),
order = c('High_confidence_doublet','Low_confidence_doublet' ) # Plot Doublets on top
) +
ggtitle("DoubletFinder doublets")
# Add a bit of transparency to Singlets
p[[1]]$layers[[1]]$aes_params$alpha = ifelse ( pbmc.doublet@meta.data$DF_confidence == "Singlet", .4, 1 )
png("data/results/PBMC_UMAP_doublets.png",
width = 6, height = 4, units = "in", res = 400)
p
dev.off()
dev.off()
png("data/results/PBMC_UMAP_doublets.png",
width = 6, height = 4, units = "in", res = 400)
p
dev.off()
p = DimPlot(
pbmc.doublet,
label = F,
group.by = "DF_confidence",
cols = c(
'Singlet' = '#28C2FF',
'High_confidence_doublet' = '#BD6B73',
'Low_confidence_doublet' = '#FFA630'
),
order = c('High_confidence_doublet','Low_confidence_doublet' ) # Plot Doublets on top
) +
ggtitle("DoubletFinder doublets")
png("data/results/PBMC_UMAP_doublets.png",
width = 6, height = 4, units = "in", res = 400)
p
dev.off()
bookdown::serve_book()
53/2700
?round
bookdown::render_book("index.Rmd", "bookdown::pdf_book")
# Render book to html
bookdown::render_book("index.Rmd", "bookdown::gitbook")
?bookdown::render_book
